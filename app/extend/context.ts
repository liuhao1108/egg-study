import { Context } from 'egg';

export default {
  getIp(this: Context) {
    return this.request.ip;
  },
};
