export default {
  formatDate(date: string) {
    const d = new Date(parseInt(date) * 1000);
    const year = d.getFullYear();
    const month = d.getMonth() + 1;
    const day = d.getDay();
    const str = `${year}-${month}-${day}`;
    return str;
  },
};
