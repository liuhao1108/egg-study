import { Controller } from 'egg';

export default class AdminController extends Controller {
  public async index() {
    const { ctx } = this;
    console.log(ctx.query.name);
    const userName: string = ctx.query.name;
    ctx.body = await ctx.service.login.welcome(userName);
  }

  public async user() {
    const { ctx } = this;
    const id: string = ctx.params.id;
    // console.log(id);
    const udata: string = await ctx.service.login.userCenter(id);
    const arr: number[] = [ 111, 222, 333 ];
    await ctx.render('user.ejs', {
      data: udata,
      list: arr,
    });
  }
}
