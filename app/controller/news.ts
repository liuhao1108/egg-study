import { Controller } from 'egg';

export default class NewsController extends Controller {
  public async index() {
    const { ctx } = this;
    const data = await ctx.service.news.getNewsList();
    const userInfo = ctx.cookies.get('username');
    console.log(ctx.authUser);
    await ctx.render('news.ejs', {
      data,
      userInfo,
    });
  }

  public async newsContent() {
    const { ctx } = this;
    const aid = ctx.query.aid;
    const data = await ctx.service.news.getContent(aid);
    await ctx.render('newscontent.ejs', {
      data: data[0],
    });
  }
}
