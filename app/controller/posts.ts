import { Controller } from 'egg';

const createRule = {
  name: 'string',
  age: {
    type: 'number',
    min: 0,
    max: 100,
  },
  sex: [ 'man', 'woman' ],
};

export default class PostsController extends Controller {
  public async index() {
    const { ctx } = this;
    ctx.body = 'posts index';
  }

  public async create() {
    const { ctx, app } = this;
    // ctx.validate(createRule, ctx.request.body);
    const error = app.validator.validate(createRule, ctx.request.body);
    if (error) {
      ctx.body = error;
      ctx.status = 422;
    } else {
      ctx.body = ctx.request.body;
    }
    ctx.status = 201;
  }
}
