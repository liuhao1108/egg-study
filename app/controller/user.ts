import { Controller } from 'egg';

export default class UserController extends Controller {
  public async index() {
    const { ctx } = this;
    const userName: string = ctx.query.name;
    ctx.body = await ctx.service.login.welcome(userName);
  }

  public async add() {
    const { ctx } = this;
    const result = await ctx.service.user.createUser({
      name: 'zhangsan',
      url: 'http://zhangsan.com',
      country: 'CN',
    });
    ctx.body = result;
  }
}
