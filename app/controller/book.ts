import { Controller } from 'egg';

export default class BookController extends Controller {
  public async index() {
    const { ctx } = this;
    const result = await ctx.service.book.select();
    await ctx.render('book.ejs', {
      result,
    });
  }

  public async apiIndex() {
    const { ctx } = this;
    const result = await ctx.service.book.select();
    ctx.body = result;
  }

  public async insert() {
    const { ctx } = this;
    const book = {
      bookName: 'js',
      bookCounts: 2,
      detail: 'test',
    };
    const result = await ctx.service.book.create(book);
    if (result.affectedRows === 1) {
      ctx.body = '插入成功';
    } else {
      ctx.body = '插入失败';
    }
  }
}
