import { Context } from 'egg';
// 这个中间件的作用是将接收到的数据再发送给客户端
export default function filter(): any {
  return async (ctx: Context, next: () => Promise<any>) => {
    ctx.socket.emit('res', 'packet received!');
    console.log('packet:', ctx.packet);
    await next();
  };
}
