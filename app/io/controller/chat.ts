import { Controller } from 'egg';

export default class ChatController extends Controller {
  public async index() {
    const { ctx } = this;
    const message = ctx.args[0];
    ctx.socket.emit('res', `Hi! I've got your message: ${message}`);
  }
}
