import { Context, Application, EggAppConfig } from 'egg';

// 自定义的中间件
export default function printDate(options: EggAppConfig['printdate'], app: Application): any {
  app.logger.warn(options.configStr);
  return async (ctx: Context, next: () => Promise<any>) => {
    ctx.logger.info(new Date());
    await next();
  };
}
