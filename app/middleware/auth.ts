import { Context } from 'egg';

// 自定义的中间件
export default function auth(): any {
  return async (ctx: Context, next: () => Promise<any>) => {
    // 设置全局变量
    ctx.state.csrf = ctx.csrf;
    await next();
  };
}
