import { Service } from 'egg';

/**
 * User Service
 */
export default class User extends Service {

  public async createUser(user: object) {
    const res = await this.ctx.model.User.create(user);
    return res;
  }
}
