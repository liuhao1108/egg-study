import { Service } from 'egg';

/**
 * News Service
 */
export default class Login extends Service {
  /**
   * getNewsList
   */
  public async getNewsList() {
    const url = this.config.api + 'appapi.php?a=getPortalList&catid=20&page=1';
    const res = await this.ctx.curl(url);
    const response = JSON.parse(res.data);
    return response.result;
  }

  /**
   * getContent
   * @param aid 文章aid
   */
  public async getContent(aid: string) {
    const url = this.config.api + 'appapi.php?a=getPortalArticle&aid=' + aid;
    const res = await this.ctx.curl(url);
    const response = JSON.parse(res.data);
    return response.result;
  }
}
