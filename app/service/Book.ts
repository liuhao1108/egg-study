import { Service } from 'egg';

/**
 * Book Service
 */
export default class Book extends Service {

  /**
   * create a book data
   */
  public async create(book) {
    const { app } = this;
    const result = await app.mysql.insert('books', book);
    return result;
  }

  public async select(options?: object) {
    const { app } = this;
    const result = await app.mysql.select('books', options);
    return result;
  }
}
