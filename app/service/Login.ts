import { Service } from 'egg';

/**
 * Login Service
 */
export default class Login extends Service {

  /**
   * welcome for you
   * @param name - your name
   */
  public async welcome(name: string) {
    return `欢迎回来${name}`;
  }

  /**
 * userCenter for you
 * @param id - your id
 */
  public async userCenter(id: string) {
    // this.logger.info(this.config.str);
    this.logger.debug('this is degug');
    this.logger.info('this is info');
    this.logger.warn('this is warn');
    this.logger.error('this is error');
    return `${id}`;
  }
}
