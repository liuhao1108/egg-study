import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router, middleware, io } = app;

  const token = middleware.jwtAuth(app.config.jwt);

  router.get('/', controller.home.index);
  router.get('/admin', controller.admin.index);
  router.get('/user/:id', controller.admin.user);
  router.get('/news', token, controller.news.index);
  router.get('/newscontent', controller.news.newsContent);
  router.post('/add', controller.home.add);
  router.get('/bookadd', controller.book.insert);
  router.get('/book', controller.book.index);
  router.get('/api/v2/book', controller.book.apiIndex);
  router.get('/useradd', controller.user.add);
  router.resources('posts', '/api/posts', controller.posts);

  io.route('chat', io.controller.chat.index);

  app.passport.mount('weibo');
};
