// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportAuth from '../../../app/middleware/auth';
import ExportErrorHandle from '../../../app/middleware/error_handle';
import ExportJwtAuth from '../../../app/middleware/jwtAuth';
import ExportPrintdate from '../../../app/middleware/printdate';

declare module 'egg' {
  interface IMiddleware {
    auth: typeof ExportAuth;
    errorHandle: typeof ExportErrorHandle;
    jwtAuth: typeof ExportJwtAuth;
    printdate: typeof ExportPrintdate;
  }
}
