// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportAdmin from '../../../app/controller/admin';
import ExportBook from '../../../app/controller/book';
import ExportHome from '../../../app/controller/home';
import ExportNews from '../../../app/controller/news';
import ExportPosts from '../../../app/controller/posts';
import ExportUser from '../../../app/controller/user';

declare module 'egg' {
  interface IController {
    admin: ExportAdmin;
    book: ExportBook;
    home: ExportHome;
    news: ExportNews;
    posts: ExportPosts;
    user: ExportUser;
  }
}
