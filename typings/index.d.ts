import 'egg';

declare module 'egg' {
  interface mysql {
    get(tableName: String, find: {}): Promise<Any>
    insert(tableName: String, find: {}): Promise<Any>
    update(tableName: String, find: {}): Promise<Any>
    delete(tableName: String, find: {}): Promise<Any>
    select(tableName: String, find?: {}): Promise<Any>

    query(sql: String, values: Any[]): Promise<Any>
  }
  interface passport {
    mount(oauth: string): Promise<Any>
  }
  interface jwt {
    sign(data: {}, secret: string): string
    verify(token: string, secret: string): any
  }
  interface validator{
    validate(rule: {}, target: {}): {}
    addRule(ruleName: string, callback: function): boolean
  }
  interface Application {
    mysql: mysql;
    passport: passport;
    jwt: jwt;
    validator: validator;
  }
}