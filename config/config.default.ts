import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1597310566355_3848';

  // add your egg config in here
  config.middleware = [ 'auth', 'errorHandle' ];

  // weibo登录
  config.passportWeibo = {
    key: '3705302392',
    secret: 'f4ad5c67d6ac54e908cf4f3b692297a6',
  };

  // validate, 参数校验器
  config.validate = {
    convert: true, // 对参数可以使用convertType规则进行类型转换
    // validateRoot: false,   // 限制被验证值必须是一个对象。
  };

  // JWT秘钥
  config.jwt = {
    secret: 'xiaofeng',
  };
  config.jwtAuth = {
    secret: 'xiaofeng',
  };

  // socket
  config.io = {
    // init: { wsEngine: 'uws' },
    init: {}, // 默认ws
    namespace: { // 命名空间
      '/': {
        connectionMiddleware: [ 'connection' ], // 连接中间件
        packetMiddleware: [ 'filter' ], // 数据包中间件
      },
    },
  };

  // 模板引擎
  config.view = {
    mapping: {
      '.ejs': 'ejs',
    },
  };

  // 关闭csrf
  config.security = {
    csrf: {
      enable: false,
    },
    domainWhiteList: [ '*' ],
  };

  // CORS
  config.cors = {
    origin: '*',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
  };

  // mysql数据库
  config.mysql = {
    client: { // 单数据库信息配置
      host: 'localhost', // host
      port: '3306', // 端口号
      user: 'liuhao', // 用户名
      password: '123456', // 密码
      database: 'ssmbuild', // 数据库名
    },
    app: true, // 是否加载到 app 上，默认开启
    agent: false, // 是否加载到 agent 上，默认关闭
  };

  // 配置sequellize
  config.sequelize = {
    dialect: 'mysql', // 数据库类型
    database: 'ssmbuild', // 数据库名称
    host: '127.0.0.1', // 数据库ip地址
    port: 3306, // 数据库端口
    username: 'liuhao', // 数据库用户名
    password: '123456', // 数据库密码
  };

  // 自定义配置项
  config.api = 'http://www.phonegap100.com/';

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
    // 中间件传值
    printdate: {
      configStr: 'this is value from config',
    },
  };

  // the return config will combines to EggAppConfig
  return {
    ...config,
    ...bizConfig,
  };
};
